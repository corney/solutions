package ru.corney.solutions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by corney on 31.07.16.
 */
public class SubwayDistanceTest {

    /**
     * Заменить своей реализацией, которая будет проходить тесты.
     *
     * Реализацию сделать в том же пакете, где находится исходный интерфейс
     */
    private final SubwayDistance subwayDistance = new SubwayDistance() {
        @Override
        public int[][] calculateDistances(int[][] crossroads) {
            return crossroads;
        }
    };

    @Before
    public void setUp() {

    }

    @Test
    public void test4x4() {
        int[][] crossroads = {
                {0, 0, 1, 0},
                {1, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 1}
        };

        int[][] sample = {
                {1, 1, 0, 1},
                {0, 1, 1, 2},
                {1, 2, 2, 1},
                {2, 2, 1, 0}
        };

        int[][] distances = subwayDistance.calculateDistances(crossroads);

        for (int i = 0; i < crossroads.length; i++) {
            Assert.assertArrayEquals(sample[i], distances[i]);
        }
    }
}
